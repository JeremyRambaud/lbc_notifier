import fs from 'fs';

export default class Parser {
  constructor() {
    this.url = null;
    this.searchId = null;
    this.parser = {};
  }

  setUrl(url) {
    if (url) {
      this.url = url;
    }
  }

  setSearchId(searchId) {
    if (searchId) {
      this.searchId = searchId;
    }
  }

  setParser(parser) {
    if (parser) {
      this.parser = parser;
    }
  }

  async parse() {
    /* eslint-disable dot-notation */
    /* eslint new-cap: ["error", { "newIsCapExceptions": ["parser"] }] */
    const parser = new this['parser'](this.url);

    const tmpDirExist = fs.existsSync('./tmp');
    if (!tmpDirExist) {
      fs.mkdirSync('./tmp');
    }

    const parsedIds = fs.existsSync(`./tmp/parsedIds_${this.searchId}_${this.parser.name}.json`) ? JSON.parse(fs.readFileSync(`./tmp/parsedIds_${this.searchId}_${this.parser.name}.json`, 'utf8')) : [];

    const ads = await parser.parse();
    const eligibleAds = [];
    const newParsedIds = [];

    Array.prototype.forEach.call(ads, (ad) => {
      if (!parsedIds.includes(ad.id)) {
        eligibleAds.push(ad);
      }

      newParsedIds.push(ad.id);
    });

    fs.writeFileSync(`./tmp/parsedIds_${this.searchId}_${this.parser.name}.json`, JSON.stringify(newParsedIds), 'utf8');

    return eligibleAds;
  }
}
