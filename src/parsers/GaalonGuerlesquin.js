import { JSDOM } from 'jsdom';

export default class GaalonGuerlesquin {
  constructor(url) {
    this.url = url;
    this.baseUrl = 'https://www.gaalon-guerlesquin.fr';
  }

  async parse() {
    const dom = await JSDOM.fromURL(this.url, {
      referrer: this.baseUrl,
    }).catch(err => console.log(err));

    const adsList = dom.window.document.querySelectorAll('.teaser');

    const ads = [];

    Array.prototype.forEach.call(adsList, (ad) => {
      const link = ad.getAttribute('data-url');
      const id = /(\d+)\?/g.exec(link);

      ads.push({
        id: parseInt(id[1], 10),
        title: `GG - ${ad.getAttribute('data-title').trim()} ${ad.querySelector('.teaser__location').textContent.trim()}`,
        image: ad.querySelector('img').getAttribute('src'),
        link,
      });
    });

    return ads;
  }
}
