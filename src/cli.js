#!/usr/bin/env node

import 'babel-polyfill';
import 'babel-core/register';
import Cron from 'node-cron';
import Parser from './Parser';
import Notifier from './Notifier';

const [,, ...args] = process.argv;

const settingsFileName = args[0];

if (settingsFileName === undefined) {
  throw Error('Config name not provided !\nUsage : lbc-parser [configName]\n  configName: searches config file name\n\n');
}

async function init() {
  let settings = require(`../config/searches/${settingsFileName}.js`); // eslint-disable-line global-require,import/no-dynamic-require
  settings = settings.default;

  let ads = [];
  const parser = new Parser();

  for (const parserSettings of settings.parsers) {
    /* eslint-disable no-await-in-loop */
    const parserClass = require(`./parsers/${parserSettings.parser}.js`); // eslint-disable-line global-require,import/no-dynamic-require

    parser.setUrl(parserSettings.url);
    parser.setSearchId(settingsFileName);
    parser.setParser(parserClass.default);
    const parsedAds = await parser.parse().catch(err => console.log(err));
    ads = ads.concat(parsedAds);
  }

  const notifier = new Notifier(settings.ifttt.event, settings.ifttt.key);

  /* eslint 'no-restricted-syntax': ['error', 'LabeledStatement', 'WithStatement'] */
  for (const ad of ads) {
    /* eslint-disable no-await-in-loop */
    await notifier.notify(ad.title, ad.link, ad.image).catch(err => console.log(err));
  }
}

Cron.schedule('* * * * *', async () => {
  console.info(`[${settingsFileName}] Parsing started ...`);
  await init();
  console.info(`[${settingsFileName}] Parsing ended !`);
});
