module.exports = {
  "extends": "airbnb-base",

  "env": {
    "es6": true,
    "node": true
  },

  "globals": {
    "document": false,
    "navigator": false,
    "window": false
    },
};
