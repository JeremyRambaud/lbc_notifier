'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _fs = require('fs');var _fs2 = _interopRequireDefault(_fs);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

Parser = function () {
  function Parser() {(0, _classCallCheck3.default)(this, Parser);
    this.url = null;
    this.searchId = null;
    this.parser = {};
  }(0, _createClass3.default)(Parser, [{ key: 'setUrl', value: function setUrl(

    url) {
      if (url) {
        this.url = url;
      }
    } }, { key: 'setSearchId', value: function setSearchId(

    searchId) {
      if (searchId) {
        this.searchId = searchId;
      }
    } }, { key: 'setParser', value: function setParser(

    parser) {
      if (parser) {
        this.parser = parser;
      }
    } }, { key: 'parse', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {var parser, tmpDirExist, parsedIds, ads, eligibleAds, newParsedIds;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:


                /* eslint-disable dot-notation */
                /* eslint new-cap: ["error", { "newIsCapExceptions": ["parser"] }] */
                parser = new this['parser'](this.url);

                tmpDirExist = _fs2.default.existsSync('./tmp');
                if (!tmpDirExist) {
                  _fs2.default.mkdirSync('./tmp');
                }

                parsedIds = _fs2.default.existsSync('./tmp/parsedIds_' + this.searchId + '_' + this.parser.name + '.json') ? JSON.parse(_fs2.default.readFileSync('./tmp/parsedIds_' + this.searchId + '_' + this.parser.name + '.json', 'utf8')) : [];_context.next = 6;return (

                  parser.parse());case 6:ads = _context.sent;
                eligibleAds = [];
                newParsedIds = [];

                Array.prototype.forEach.call(ads, function (ad) {
                  if (!parsedIds.includes(ad.id)) {
                    eligibleAds.push(ad);
                  }

                  newParsedIds.push(ad.id);
                });

                _fs2.default.writeFileSync('./tmp/parsedIds_' + this.searchId + '_' + this.parser.name + '.json', JSON.stringify(newParsedIds), 'utf8');return _context.abrupt('return',

                eligibleAds);case 12:case 'end':return _context.stop();}}}, _callee, this);}));function parse() {return _ref.apply(this, arguments);}return parse;}() }]);return Parser;}();exports.default = Parser;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9QYXJzZXIuanMiXSwibmFtZXMiOlsiUGFyc2VyIiwidXJsIiwic2VhcmNoSWQiLCJwYXJzZXIiLCJ0bXBEaXJFeGlzdCIsImZzIiwiZXhpc3RzU3luYyIsIm1rZGlyU3luYyIsInBhcnNlZElkcyIsIm5hbWUiLCJKU09OIiwicGFyc2UiLCJyZWFkRmlsZVN5bmMiLCJhZHMiLCJlbGlnaWJsZUFkcyIsIm5ld1BhcnNlZElkcyIsIkFycmF5IiwicHJvdG90eXBlIiwiZm9yRWFjaCIsImNhbGwiLCJhZCIsImluY2x1ZGVzIiwiaWQiLCJwdXNoIiwid3JpdGVGaWxlU3luYyIsInN0cmluZ2lmeSJdLCJtYXBwaW5ncyI6IjZrQkFBQSx3Qjs7QUFFcUJBLE07QUFDbkIsb0JBQWM7QUFDWixTQUFLQyxHQUFMLEdBQVcsSUFBWDtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxTQUFLQyxNQUFMLEdBQWMsRUFBZDtBQUNELEc7O0FBRU1GLE8sRUFBSztBQUNWLFVBQUlBLEdBQUosRUFBUztBQUNQLGFBQUtBLEdBQUwsR0FBV0EsR0FBWDtBQUNEO0FBQ0YsSzs7QUFFV0MsWSxFQUFVO0FBQ3BCLFVBQUlBLFFBQUosRUFBYztBQUNaLGFBQUtBLFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0Q7QUFDRixLOztBQUVTQyxVLEVBQVE7QUFDaEIsVUFBSUEsTUFBSixFQUFZO0FBQ1YsYUFBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0Q7QUFDRixLOzs7QUFHQztBQUNBO0FBQ01BLHNCLEdBQVMsSUFBSSxLQUFLLFFBQUwsQ0FBSixDQUFtQixLQUFLRixHQUF4QixDOztBQUVURywyQixHQUFjQyxhQUFHQyxVQUFILENBQWMsT0FBZCxDO0FBQ3BCLG9CQUFJLENBQUNGLFdBQUwsRUFBa0I7QUFDaEJDLCtCQUFHRSxTQUFILENBQWEsT0FBYjtBQUNEOztBQUVLQyx5QixHQUFZSCxhQUFHQyxVQUFILHNCQUFpQyxLQUFLSixRQUF0QyxTQUFrRCxLQUFLQyxNQUFMLENBQVlNLElBQTlELGNBQTZFQyxLQUFLQyxLQUFMLENBQVdOLGFBQUdPLFlBQUgsc0JBQW1DLEtBQUtWLFFBQXhDLFNBQW9ELEtBQUtDLE1BQUwsQ0FBWU0sSUFBaEUsWUFBNkUsTUFBN0UsQ0FBWCxDQUE3RSxHQUFnTCxFOztBQUVoTE4seUJBQU9RLEtBQVAsRSxTQUFaRSxHO0FBQ0FDLDJCLEdBQWMsRTtBQUNkQyw0QixHQUFlLEU7O0FBRXJCQyxzQkFBTUMsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCTixHQUE3QixFQUFrQyxVQUFDTyxFQUFELEVBQVE7QUFDeEMsc0JBQUksQ0FBQ1osVUFBVWEsUUFBVixDQUFtQkQsR0FBR0UsRUFBdEIsQ0FBTCxFQUFnQztBQUM5QlIsZ0NBQVlTLElBQVosQ0FBaUJILEVBQWpCO0FBQ0Q7O0FBRURMLCtCQUFhUSxJQUFiLENBQWtCSCxHQUFHRSxFQUFyQjtBQUNELGlCQU5EOztBQVFBakIsNkJBQUdtQixhQUFILHNCQUFvQyxLQUFLdEIsUUFBekMsU0FBcUQsS0FBS0MsTUFBTCxDQUFZTSxJQUFqRSxZQUE4RUMsS0FBS2UsU0FBTCxDQUFlVixZQUFmLENBQTlFLEVBQTRHLE1BQTVHLEU7O0FBRU9ELDJCLG1MQW5EVWQsTSIsImZpbGUiOiJQYXJzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZnMgZnJvbSAnZnMnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQYXJzZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnVybCA9IG51bGw7XG4gICAgdGhpcy5zZWFyY2hJZCA9IG51bGw7XG4gICAgdGhpcy5wYXJzZXIgPSB7fTtcbiAgfVxuXG4gIHNldFVybCh1cmwpIHtcbiAgICBpZiAodXJsKSB7XG4gICAgICB0aGlzLnVybCA9IHVybDtcbiAgICB9XG4gIH1cblxuICBzZXRTZWFyY2hJZChzZWFyY2hJZCkge1xuICAgIGlmIChzZWFyY2hJZCkge1xuICAgICAgdGhpcy5zZWFyY2hJZCA9IHNlYXJjaElkO1xuICAgIH1cbiAgfVxuXG4gIHNldFBhcnNlcihwYXJzZXIpIHtcbiAgICBpZiAocGFyc2VyKSB7XG4gICAgICB0aGlzLnBhcnNlciA9IHBhcnNlcjtcbiAgICB9XG4gIH1cblxuICBhc3luYyBwYXJzZSgpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBkb3Qtbm90YXRpb24gKi9cbiAgICAvKiBlc2xpbnQgbmV3LWNhcDogW1wiZXJyb3JcIiwgeyBcIm5ld0lzQ2FwRXhjZXB0aW9uc1wiOiBbXCJwYXJzZXJcIl0gfV0gKi9cbiAgICBjb25zdCBwYXJzZXIgPSBuZXcgdGhpc1sncGFyc2VyJ10odGhpcy51cmwpO1xuXG4gICAgY29uc3QgdG1wRGlyRXhpc3QgPSBmcy5leGlzdHNTeW5jKCcuL3RtcCcpO1xuICAgIGlmICghdG1wRGlyRXhpc3QpIHtcbiAgICAgIGZzLm1rZGlyU3luYygnLi90bXAnKTtcbiAgICB9XG5cbiAgICBjb25zdCBwYXJzZWRJZHMgPSBmcy5leGlzdHNTeW5jKGAuL3RtcC9wYXJzZWRJZHNfJHt0aGlzLnNlYXJjaElkfV8ke3RoaXMucGFyc2VyLm5hbWV9Lmpzb25gKSA/IEpTT04ucGFyc2UoZnMucmVhZEZpbGVTeW5jKGAuL3RtcC9wYXJzZWRJZHNfJHt0aGlzLnNlYXJjaElkfV8ke3RoaXMucGFyc2VyLm5hbWV9Lmpzb25gLCAndXRmOCcpKSA6IFtdO1xuXG4gICAgY29uc3QgYWRzID0gYXdhaXQgcGFyc2VyLnBhcnNlKCk7XG4gICAgY29uc3QgZWxpZ2libGVBZHMgPSBbXTtcbiAgICBjb25zdCBuZXdQYXJzZWRJZHMgPSBbXTtcblxuICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoYWRzLCAoYWQpID0+IHtcbiAgICAgIGlmICghcGFyc2VkSWRzLmluY2x1ZGVzKGFkLmlkKSkge1xuICAgICAgICBlbGlnaWJsZUFkcy5wdXNoKGFkKTtcbiAgICAgIH1cblxuICAgICAgbmV3UGFyc2VkSWRzLnB1c2goYWQuaWQpO1xuICAgIH0pO1xuXG4gICAgZnMud3JpdGVGaWxlU3luYyhgLi90bXAvcGFyc2VkSWRzXyR7dGhpcy5zZWFyY2hJZH1fJHt0aGlzLnBhcnNlci5uYW1lfS5qc29uYCwgSlNPTi5zdHJpbmdpZnkobmV3UGFyc2VkSWRzKSwgJ3V0ZjgnKTtcblxuICAgIHJldHVybiBlbGlnaWJsZUFkcztcbiAgfVxufVxuIl19