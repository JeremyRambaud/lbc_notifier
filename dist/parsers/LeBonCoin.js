'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _jsdom = require('jsdom');function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

LeBonCoin = function () {
  function LeBonCoin(url) {(0, _classCallCheck3.default)(this, LeBonCoin);
    this.url = url;
    this.baseUrl = 'https://leboncoin.fr/';
  }(0, _createClass3.default)(LeBonCoin, [{ key: 'parse', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {var dom, data, ads;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (


                  _jsdom.JSDOM.fromURL(this.url, {
                    referrer: this.baseUrl,
                    runScripts: 'dangerously' }).
                  catch(function (err) {return console.log(err);}));case 2:dom = _context.sent;

                data = dom.window.FLUX_STATE.adSearch.data.ads;

                ads = [];

                Array.prototype.forEach.call(data, function (ad) {
                  // const adDate = new Date(ad.index_date);

                  ads.push({
                    id: ad.list_id,
                    title: 'LBC - ' + ad.subject,
                    // date: `${adDate.getHours()}:${adDate.getMinutes()}`,
                    // location: ad.location.city_label,
                    // price: ad.price[0],
                    // category: ad.category_name,
                    image: ad.images.nb_images > 0 ? ad.images.thumb_url : null,
                    link: ad.url });

                });return _context.abrupt('return',

                ads);case 7:case 'end':return _context.stop();}}}, _callee, this);}));function parse() {return _ref.apply(this, arguments);}return parse;}() }]);return LeBonCoin;}();exports.default = LeBonCoin;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYXJzZXJzL0xlQm9uQ29pbi5qcyJdLCJuYW1lcyI6WyJMZUJvbkNvaW4iLCJ1cmwiLCJiYXNlVXJsIiwiSlNET00iLCJmcm9tVVJMIiwicmVmZXJyZXIiLCJydW5TY3JpcHRzIiwiY2F0Y2giLCJjb25zb2xlIiwibG9nIiwiZXJyIiwiZG9tIiwiZGF0YSIsIndpbmRvdyIsIkZMVVhfU1RBVEUiLCJhZFNlYXJjaCIsImFkcyIsIkFycmF5IiwicHJvdG90eXBlIiwiZm9yRWFjaCIsImNhbGwiLCJhZCIsInB1c2giLCJpZCIsImxpc3RfaWQiLCJ0aXRsZSIsInN1YmplY3QiLCJpbWFnZSIsImltYWdlcyIsIm5iX2ltYWdlcyIsInRodW1iX3VybCIsImxpbmsiXSwibWFwcGluZ3MiOiI2a0JBQUEsOEI7O0FBRXFCQSxTO0FBQ25CLHFCQUFZQyxHQUFaLEVBQWlCO0FBQ2YsU0FBS0EsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLHVCQUFmO0FBQ0QsRzs7O0FBR21CQywrQkFBTUMsT0FBTixDQUFjLEtBQUtILEdBQW5CLEVBQXdCO0FBQ3hDSSw4QkFBVSxLQUFLSCxPQUR5QjtBQUV4Q0ksZ0NBQVksYUFGNEIsRUFBeEI7QUFHZkMsdUJBSGUsQ0FHVCx1QkFBT0MsUUFBUUMsR0FBUixDQUFZQyxHQUFaLENBQVAsRUFIUyxDLFNBQVpDLEc7O0FBS0FDLG9CLEdBQU9ELElBQUlFLE1BQUosQ0FBV0MsVUFBWCxDQUFzQkMsUUFBdEIsQ0FBK0JILElBQS9CLENBQW9DSSxHOztBQUUzQ0EsbUIsR0FBTSxFOztBQUVaQyxzQkFBTUMsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCUixJQUE3QixFQUFtQyxVQUFDUyxFQUFELEVBQVE7QUFDekM7O0FBRUFMLHNCQUFJTSxJQUFKLENBQVM7QUFDUEMsd0JBQUlGLEdBQUdHLE9BREE7QUFFUEMsc0NBQWdCSixHQUFHSyxPQUZaO0FBR1A7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsMkJBQU9OLEdBQUdPLE1BQUgsQ0FBVUMsU0FBVixHQUFzQixDQUF0QixHQUEwQlIsR0FBR08sTUFBSCxDQUFVRSxTQUFwQyxHQUFnRCxJQVBoRDtBQVFQQywwQkFBTVYsR0FBR3BCLEdBUkYsRUFBVDs7QUFVRCxpQkFiRCxFOztBQWVPZSxtQixxTEEvQlVoQixTIiwiZmlsZSI6IkxlQm9uQ29pbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEpTRE9NIH0gZnJvbSAnanNkb20nO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMZUJvbkNvaW4ge1xuICBjb25zdHJ1Y3Rvcih1cmwpIHtcbiAgICB0aGlzLnVybCA9IHVybDtcbiAgICB0aGlzLmJhc2VVcmwgPSAnaHR0cHM6Ly9sZWJvbmNvaW4uZnIvJztcbiAgfVxuXG4gIGFzeW5jIHBhcnNlKCkge1xuICAgIGNvbnN0IGRvbSA9IGF3YWl0IEpTRE9NLmZyb21VUkwodGhpcy51cmwsIHtcbiAgICAgIHJlZmVycmVyOiB0aGlzLmJhc2VVcmwsXG4gICAgICBydW5TY3JpcHRzOiAnZGFuZ2Vyb3VzbHknLFxuICAgIH0pLmNhdGNoKGVyciA9PiBjb25zb2xlLmxvZyhlcnIpKTtcblxuICAgIGNvbnN0IGRhdGEgPSBkb20ud2luZG93LkZMVVhfU1RBVEUuYWRTZWFyY2guZGF0YS5hZHM7XG5cbiAgICBjb25zdCBhZHMgPSBbXTtcblxuICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoZGF0YSwgKGFkKSA9PiB7XG4gICAgICAvLyBjb25zdCBhZERhdGUgPSBuZXcgRGF0ZShhZC5pbmRleF9kYXRlKTtcblxuICAgICAgYWRzLnB1c2goe1xuICAgICAgICBpZDogYWQubGlzdF9pZCxcbiAgICAgICAgdGl0bGU6IGBMQkMgLSAke2FkLnN1YmplY3R9YCxcbiAgICAgICAgLy8gZGF0ZTogYCR7YWREYXRlLmdldEhvdXJzKCl9OiR7YWREYXRlLmdldE1pbnV0ZXMoKX1gLFxuICAgICAgICAvLyBsb2NhdGlvbjogYWQubG9jYXRpb24uY2l0eV9sYWJlbCxcbiAgICAgICAgLy8gcHJpY2U6IGFkLnByaWNlWzBdLFxuICAgICAgICAvLyBjYXRlZ29yeTogYWQuY2F0ZWdvcnlfbmFtZSxcbiAgICAgICAgaW1hZ2U6IGFkLmltYWdlcy5uYl9pbWFnZXMgPiAwID8gYWQuaW1hZ2VzLnRodW1iX3VybCA6IG51bGwsXG4gICAgICAgIGxpbms6IGFkLnVybCxcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIGFkcztcbiAgfVxufVxuIl19