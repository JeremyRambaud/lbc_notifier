'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _jsdom = require('jsdom');function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

BrasImmobilier = function () {
  function BrasImmobilier(url) {(0, _classCallCheck3.default)(this, BrasImmobilier);
    this.url = url;
    this.baseUrl = 'https://www.bras-immobilier.fr';
  }(0, _createClass3.default)(BrasImmobilier, [{ key: 'parse', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {var dom, adsList, ads;return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (


                  _jsdom.JSDOM.fromURL(this.url, {
                    referrer: this.baseUrl }).
                  catch(function (err) {return console.log(err);}));case 2:dom = _context.sent;

                adsList = dom.window.document.querySelectorAll('.node-immobilier');

                ads = [];

                Array.prototype.forEach.call(adsList, function (ad) {
                  var link = ad.getAttribute('data-url');
                  var id = /(\d+)\?/g.exec(link);

                  ads.push({
                    id: parseInt(id[1], 10),
                    title: 'BI - ' + ad.getAttribute('data-title').trim() + ' ' + ad.querySelector('.infos .secteur').textContent.trim(),
                    image: ad.querySelector('img').getAttribute('src'),
                    link: link });

                });return _context.abrupt('return',

                ads);case 7:case 'end':return _context.stop();}}}, _callee, this);}));function parse() {return _ref.apply(this, arguments);}return parse;}() }]);return BrasImmobilier;}();exports.default = BrasImmobilier;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYXJzZXJzL0JyYXNJbW1vYmlsaWVyLmpzIl0sIm5hbWVzIjpbIkJyYXNJbW1vYmlsaWVyIiwidXJsIiwiYmFzZVVybCIsIkpTRE9NIiwiZnJvbVVSTCIsInJlZmVycmVyIiwiY2F0Y2giLCJjb25zb2xlIiwibG9nIiwiZXJyIiwiZG9tIiwiYWRzTGlzdCIsIndpbmRvdyIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsImFkcyIsIkFycmF5IiwicHJvdG90eXBlIiwiZm9yRWFjaCIsImNhbGwiLCJhZCIsImxpbmsiLCJnZXRBdHRyaWJ1dGUiLCJpZCIsImV4ZWMiLCJwdXNoIiwicGFyc2VJbnQiLCJ0aXRsZSIsInRyaW0iLCJxdWVyeVNlbGVjdG9yIiwidGV4dENvbnRlbnQiLCJpbWFnZSJdLCJtYXBwaW5ncyI6IjZrQkFBQSw4Qjs7QUFFcUJBLGM7QUFDbkIsMEJBQVlDLEdBQVosRUFBaUI7QUFDZixTQUFLQSxHQUFMLEdBQVdBLEdBQVg7QUFDQSxTQUFLQyxPQUFMLEdBQWUsZ0NBQWY7QUFDRCxHOzs7QUFHbUJDLCtCQUFNQyxPQUFOLENBQWMsS0FBS0gsR0FBbkIsRUFBd0I7QUFDeENJLDhCQUFVLEtBQUtILE9BRHlCLEVBQXhCO0FBRWZJLHVCQUZlLENBRVQsdUJBQU9DLFFBQVFDLEdBQVIsQ0FBWUMsR0FBWixDQUFQLEVBRlMsQyxTQUFaQyxHOztBQUlBQyx1QixHQUFVRCxJQUFJRSxNQUFKLENBQVdDLFFBQVgsQ0FBb0JDLGdCQUFwQixDQUFxQyxrQkFBckMsQzs7QUFFVkMsbUIsR0FBTSxFOztBQUVaQyxzQkFBTUMsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCUixPQUE3QixFQUFzQyxVQUFDUyxFQUFELEVBQVE7QUFDNUMsc0JBQU1DLE9BQU9ELEdBQUdFLFlBQUgsQ0FBZ0IsVUFBaEIsQ0FBYjtBQUNBLHNCQUFNQyxLQUFLLFdBQVdDLElBQVgsQ0FBZ0JILElBQWhCLENBQVg7O0FBRUFOLHNCQUFJVSxJQUFKLENBQVM7QUFDUEYsd0JBQUlHLFNBQVNILEdBQUcsQ0FBSCxDQUFULEVBQWdCLEVBQWhCLENBREc7QUFFUEkscUNBQWVQLEdBQUdFLFlBQUgsQ0FBZ0IsWUFBaEIsRUFBOEJNLElBQTlCLEVBQWYsU0FBdURSLEdBQUdTLGFBQUgsQ0FBaUIsaUJBQWpCLEVBQW9DQyxXQUFwQyxDQUFnREYsSUFBaEQsRUFGaEQ7QUFHUEcsMkJBQU9YLEdBQUdTLGFBQUgsQ0FBaUIsS0FBakIsRUFBd0JQLFlBQXhCLENBQXFDLEtBQXJDLENBSEE7QUFJUEQsOEJBSk8sRUFBVDs7QUFNRCxpQkFWRCxFOztBQVlPTixtQiwwTEEzQlVmLGMiLCJmaWxlIjoiQnJhc0ltbW9iaWxpZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBKU0RPTSB9IGZyb20gJ2pzZG9tJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnJhc0ltbW9iaWxpZXIge1xuICBjb25zdHJ1Y3Rvcih1cmwpIHtcbiAgICB0aGlzLnVybCA9IHVybDtcbiAgICB0aGlzLmJhc2VVcmwgPSAnaHR0cHM6Ly93d3cuYnJhcy1pbW1vYmlsaWVyLmZyJztcbiAgfVxuXG4gIGFzeW5jIHBhcnNlKCkge1xuICAgIGNvbnN0IGRvbSA9IGF3YWl0IEpTRE9NLmZyb21VUkwodGhpcy51cmwsIHtcbiAgICAgIHJlZmVycmVyOiB0aGlzLmJhc2VVcmwsXG4gICAgfSkuY2F0Y2goZXJyID0+IGNvbnNvbGUubG9nKGVycikpO1xuXG4gICAgY29uc3QgYWRzTGlzdCA9IGRvbS53aW5kb3cuZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm5vZGUtaW1tb2JpbGllcicpO1xuXG4gICAgY29uc3QgYWRzID0gW107XG5cbiAgICBBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKGFkc0xpc3QsIChhZCkgPT4ge1xuICAgICAgY29uc3QgbGluayA9IGFkLmdldEF0dHJpYnV0ZSgnZGF0YS11cmwnKTtcbiAgICAgIGNvbnN0IGlkID0gLyhcXGQrKVxcPy9nLmV4ZWMobGluayk7XG5cbiAgICAgIGFkcy5wdXNoKHtcbiAgICAgICAgaWQ6IHBhcnNlSW50KGlkWzFdLCAxMCksXG4gICAgICAgIHRpdGxlOiBgQkkgLSAke2FkLmdldEF0dHJpYnV0ZSgnZGF0YS10aXRsZScpLnRyaW0oKX0gJHthZC5xdWVyeVNlbGVjdG9yKCcuaW5mb3MgLnNlY3RldXInKS50ZXh0Q29udGVudC50cmltKCl9YCxcbiAgICAgICAgaW1hZ2U6IGFkLnF1ZXJ5U2VsZWN0b3IoJ2ltZycpLmdldEF0dHJpYnV0ZSgnc3JjJyksXG4gICAgICAgIGxpbmssXG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIHJldHVybiBhZHM7XG4gIH1cbn1cbiJdfQ==