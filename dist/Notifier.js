'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _regenerator = require('babel-runtime/regenerator');var _regenerator2 = _interopRequireDefault(_regenerator);var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);var _createClass2 = require('babel-runtime/helpers/createClass');var _createClass3 = _interopRequireDefault(_createClass2);var _requestPromiseNative = require('request-promise-native');var _requestPromiseNative2 = _interopRequireDefault(_requestPromiseNative);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var

Notifier = function () {
  function Notifier(eventName, key) {(0, _classCallCheck3.default)(this, Notifier);
    this.eventName = eventName || null;
    this.key = key || null;
  }(0, _createClass3.default)(Notifier, [{ key: 'notify', value: function () {var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(













      value1, value2, value3) {return _regenerator2.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:return _context.abrupt('return',
                _requestPromiseNative2.default.post('https://maker.ifttt.com/trigger/' + this.eventName + '/with/key/' + this.key, {
                  body: {
                    value1: value1,
                    value2: value2,
                    value3: value3 },

                  json: true },
                function (err, httpResponse, body) {
                  if (err) {
                    throw Error('Error: ' + err + '\nStatusCode: ' + (httpResponse && httpResponse.statusCode) + '\nBody: ' + body);
                  }
                }));case 1:case 'end':return _context.stop();}}}, _callee, this);}));function notify(_x, _x2, _x3) {return _ref.apply(this, arguments);}return notify;}() }, { key: 'setEventName', set: function set(eventName) {if (eventName) {this.eventName = eventName;}} }, { key: 'setKey', set: function set(key) {if (key) {this.key = key;}} }]);return Notifier;}();exports.default = Notifier;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9Ob3RpZmllci5qcyJdLCJuYW1lcyI6WyJOb3RpZmllciIsImV2ZW50TmFtZSIsImtleSIsInZhbHVlMSIsInZhbHVlMiIsInZhbHVlMyIsInJlcXVlc3QiLCJwb3N0IiwiYm9keSIsImpzb24iLCJlcnIiLCJodHRwUmVzcG9uc2UiLCJFcnJvciIsInN0YXR1c0NvZGUiXSwibWFwcGluZ3MiOiI2a0JBQUEsOEQ7O0FBRXFCQSxRO0FBQ25CLG9CQUFZQyxTQUFaLEVBQXVCQyxHQUF2QixFQUE0QjtBQUMxQixTQUFLRCxTQUFMLEdBQWlCQSxhQUFhLElBQTlCO0FBQ0EsU0FBS0MsR0FBTCxHQUFXQSxPQUFPLElBQWxCO0FBQ0QsRzs7Ozs7Ozs7Ozs7Ozs7QUFjWUMsWSxFQUFRQyxNLEVBQVFDLE07QUFDcEJDLCtDQUFRQyxJQUFSLHNDQUFnRCxLQUFLTixTQUFyRCxrQkFBMkUsS0FBS0MsR0FBaEYsRUFBdUY7QUFDNUZNLHdCQUFNO0FBQ0pMLGtDQURJO0FBRUpDLGtDQUZJO0FBR0pDLGtDQUhJLEVBRHNGOztBQU01Rkksd0JBQU0sSUFOc0YsRUFBdkY7QUFPSiwwQkFBQ0MsR0FBRCxFQUFNQyxZQUFOLEVBQW9CSCxJQUFwQixFQUE2QjtBQUM5QixzQkFBSUUsR0FBSixFQUFTO0FBQ1AsMEJBQU1FLGtCQUFnQkYsR0FBaEIsdUJBQW9DQyxnQkFBZ0JBLGFBQWFFLFVBQWpFLGlCQUFzRkwsSUFBdEYsQ0FBTjtBQUNEO0FBQ0YsaUJBWE0sQyxvTUFiUVAsUyxFQUFXLENBQzFCLElBQUlBLFNBQUosRUFBZSxDQUNiLEtBQUtBLFNBQUwsR0FBaUJBLFNBQWpCLENBQ0QsQ0FDRixDLHVDQUVVQyxHLEVBQUssQ0FDZCxJQUFJQSxHQUFKLEVBQVMsQ0FDUCxLQUFLQSxHQUFMLEdBQVdBLEdBQVgsQ0FDRCxDQUNGLEMsMkNBaEJrQkYsUSIsImZpbGUiOiJOb3RpZmllci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCByZXF1ZXN0IGZyb20gJ3JlcXVlc3QtcHJvbWlzZS1uYXRpdmUnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOb3RpZmllciB7XG4gIGNvbnN0cnVjdG9yKGV2ZW50TmFtZSwga2V5KSB7XG4gICAgdGhpcy5ldmVudE5hbWUgPSBldmVudE5hbWUgfHwgbnVsbDtcbiAgICB0aGlzLmtleSA9IGtleSB8fCBudWxsO1xuICB9XG5cbiAgc2V0IHNldEV2ZW50TmFtZShldmVudE5hbWUpIHtcbiAgICBpZiAoZXZlbnROYW1lKSB7XG4gICAgICB0aGlzLmV2ZW50TmFtZSA9IGV2ZW50TmFtZTtcbiAgICB9XG4gIH1cblxuICBzZXQgc2V0S2V5KGtleSkge1xuICAgIGlmIChrZXkpIHtcbiAgICAgIHRoaXMua2V5ID0ga2V5O1xuICAgIH1cbiAgfVxuXG4gIGFzeW5jIG5vdGlmeSh2YWx1ZTEsIHZhbHVlMiwgdmFsdWUzKSB7XG4gICAgcmV0dXJuIHJlcXVlc3QucG9zdChgaHR0cHM6Ly9tYWtlci5pZnR0dC5jb20vdHJpZ2dlci8ke3RoaXMuZXZlbnROYW1lfS93aXRoL2tleS8ke3RoaXMua2V5fWAsIHtcbiAgICAgIGJvZHk6IHtcbiAgICAgICAgdmFsdWUxLFxuICAgICAgICB2YWx1ZTIsXG4gICAgICAgIHZhbHVlMyxcbiAgICAgIH0sXG4gICAgICBqc29uOiB0cnVlLFxuICAgIH0sIChlcnIsIGh0dHBSZXNwb25zZSwgYm9keSkgPT4ge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICB0aHJvdyBFcnJvcihgRXJyb3I6ICR7ZXJyfVxcblN0YXR1c0NvZGU6ICR7aHR0cFJlc3BvbnNlICYmIGh0dHBSZXNwb25zZS5zdGF0dXNDb2RlfVxcbkJvZHk6ICR7Ym9keX1gKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxufVxuIl19